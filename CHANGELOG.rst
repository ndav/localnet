=========
Changelog
=========
All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_,
and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

[0.2.0] - 2018-09-30
++++++++++++++++++++

Changed
-------

- Don't import ctypes until we need it.
- Catch exception from all_ifaces() in _main so we can exit correctly.
- Add LICENSE file
- Fix this CHANGELOG
  
[0.1.0] - 2018-09-30
++++++++++++++++++++

Added
-----

- CHANGELOG.rst
- MANIFEST.in
- README.rst
- localnet.py
- setup.cfg
- setup.py
- .gitignore
